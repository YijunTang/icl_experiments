import logging
from typing import List

from artiq.coredevice.core import Core
from artiq.coredevice.zotino import Zotino
from artiq.experiment import delay_mu
from artiq.experiment import kernel
from artiq.experiment import now_mu
from artiq.experiment import portable
from artiq.experiment import TFloat
from artiq.experiment import TInt32
from artiq.experiment import TList
from ndscan.experiment import Fragment

from device_db_config.configuration import VoltageControlledCurrentSupply

logger = logging.getLogger(__name__)


class SetAnalogCurrentSupplies(Fragment):
    """
    Set multiple current supplies that are controlled by a analog voltages.
    The supplies must all be controlled by the same Zotino
    """

    def build_fragment(self, current_configs: List[VoltageControlledCurrentSupply]):
        self.setattr_device("core")
        self.core: Core

        self.current_configs = current_configs

        assert all(
            [c.zotino == current_configs[0].zotino for c in current_configs]
        ), "All current drivers must use the same Zotino"

        self.zotino = self.get_device(self.current_configs[0].zotino)
        self.zotino: Zotino

        self.zotino_channels = [c.zotino_channel for c in current_configs]

        # %% Kernel variables
        self.first_run = True
        self.debug_enabled = logger.isEnabledFor(logging.DEBUG)
        self.num_supplies = len(current_configs)

        # %% Kernel invariants
        kernel_invariants = getattr(self, "kernel_invariants", set())
        self.kernel_invariants = kernel_invariants | {
            "debug_enabled",
            "num_supplies",
            "current_configs",
            "zotino",
            "zotino_channels",
        }

    @kernel
    def device_setup(self) -> None:
        if self.first_run:
            if self.debug_enabled:
                logger.info("Initiating Zotino %s", self.zotino)

            self.core.break_realtime()
            self.zotino.init()

            self.first_run = False

        self.device_setup_subfragments()

    @portable
    def _single_current_to_volts(self, current: TFloat, current_supply_idx: TInt32):
        return current / self.current_configs[current_supply_idx].gain

    @portable
    def _currents_to_volts(self, currents: TList(TFloat), voltages_out: TList(TFloat)):
        if len(currents) != len(self.current_configs):
            raise ValueError("Wrong number of currents")

        if len(currents) != len(voltages_out):
            raise ValueError("Output array is wrong size")

        for i in range(len(self.current_configs)):
            voltages_out[i] = self._single_current_to_volts(currents[i], i)

    @kernel
    def set_currents(self, currents: TList(TFloat)):
        """
        Set currents in amps.

        This method does not advance the timeline but does require at least
        1.5us + 808ns * len(currents) on a Kasli 1.x as SPI events are written
        into the past.
        """

        voltages = [0.0] * len(self.current_configs)

        self._currents_to_volts(currents, voltages)

        if self.debug_enabled:
            logger.info(
                "Setting currents = %s with voltages = %s on channels %s",
                currents,
                voltages,
                self.zotino_channels,
            )

        self.zotino.set_dac(voltages, self.zotino_channels)

    @kernel
    def set_currents_ramping(
        self,
        currents_start: TList(TFloat),
        currents_end: TList(TFloat),
        duration: TFloat,
        ramp_step: TFloat = 1 / 75e3,
    ):
        """
        Queue a linear ramp of the currents controlled by this object

        This method will write lots of RTIO events for the `duration` of the
        ramp and will advance the timeline until the end of the ramp. It will
        also require quite a lot of time to compute and queue the ramp, so users
        should consider DMA if performance is limiting.

        Note that `time_step` will be approximate - this method will ensure that
        initial and final writes occurs at the start and end of the `duration`
        period, with `time_step` varied slightly to ensure that. Note also that
        this means you cannot immediately start a new ramp when the old one ends
        - it must be spaced at least one Zotino write away
        (`1.5us + 808ns * len(currents)` at time of writing).

        Args:
            currents_start (TList): List of starting currents / A

            currents_end (TList): List of ending currents / A

            duration (TFloat): Time to perform the ramp for

            ramp_step (TFloat, optional):
                Timestamp of RTIO writes / s. Defaults to `1/75e3` since the
                Zotino has a 75 kHz low-pass filter.
        """

        if self.debug_enabled:
            logger.info("Starting ramp for %.3f ms", 1e3 * duration)

        # Compute grid for writes
        num_points = 1 + int(duration // ramp_step)
        actual_time_step_mu = self.core.seconds_to_mu(duration / float(num_points))

        current_steps = [0.0] * self.num_supplies
        for i_supply in range(self.num_supplies):
            current_steps[i_supply] = (
                currents_end[i_supply] - currents_start[i_supply]
            ) / float(num_points - 1)

        # Here we convert the current steps to voltage steps. This assumes that
        # the current to voltage conversion function _currents_to_volts is
        # linear. If this is not true, this will break. This is tested in
        # `test_current_to_volts_convertion_is_linear`. Note that we don't
        # calculate the voltage steps in machine units. That would be vulnerable
        # to rounding errors. This means that we have to call the zotino's
        # "voltage_to_mu" function for each write which is wasteful, but assures
        # that we actually get the currents we expect.
        voltage_steps = [0.0] * self.num_supplies
        for i_supply in range(self.num_supplies):
            voltage_steps[i_supply] = self._single_current_to_volts(
                current_steps[i_supply], i_supply
            )

        # Calculate the starting voltages
        voltages_now = [0.0] * self.num_supplies
        for i_supply in range(self.num_supplies):
            voltages_now[i_supply] = self._single_current_to_volts(
                currents_start[i_supply], i_supply
            )

        if self.debug_enabled:
            logger.info(
                "Precomputation completed: %d points with steps of %s A = %s V",
                num_points,
                current_steps,
                voltage_steps,
            )

        # Queue the points, including an initial and final point
        for _ in range(num_points):
            # Set voltages
            self.zotino.set_dac(voltages_now, self.zotino_channels)

            # Calculate next voltages
            for i_supply in range(self.num_supplies):
                voltages_now[i_supply] += voltage_steps[i_supply]

            delay_mu(actual_time_step_mu)

        if self.debug_enabled:
            logger.info("RTIO events queued - now_mu() = %d", now_mu())
