from ndscan.experiment.entry_point import make_fragment_scan_exp

from repository.lib.fragments.set_eom_sidebands import SetEOMSidebandsFrag

SetEOMSidebands = make_fragment_scan_exp(SetEOMSidebandsFrag)
